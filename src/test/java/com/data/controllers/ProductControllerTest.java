package com.data.controllers;

import com.data.models.Product;
import com.data.services.ProductService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductControllerTest {

    @Mock
    private ProductService productService;

    @InjectMocks
    private ProductController productController;

    @Test
    @DisplayName("Should return all products")
    void getAllProductShouldReturnAllProducts() {
        List<Product> products = Arrays.asList(new Product(), new Product());
        when(productService.findAllProducts()).thenReturn(products);

        List<Product> result = productController.getAllProduct();

        assertEquals(products, result);
    }
}